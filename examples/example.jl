using SuspScreen

using CSV 
using DataFrames
using MS_Import



##########################################################

path2sus = "/Path/to/SusList.csv"
mode = "POSITIVE"
source = "ESI"
    

# File import
pathin="/path/to/XML_file"
filenames=["XML_file.mzXML"]
mz_thresh = [100,400]
int_thresh = 1000  
    
chrom=import_files(pathin,filenames,mz_thresh,int_thresh)


# Converting the enrties 

sus_e = suslist2entries(path2sus,mode,source);
#println(sus_e)

# Suspect Screening 
mass_tol = 0.05
min_int = 2000
iso_depth = 5
rt_width = 0.5

table = suspect_screening(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)

m = split(filenames[1],".")
output=string(pathin,"/",m[1],"_SusScreenReport.csv")
CSV.write(output,table)

